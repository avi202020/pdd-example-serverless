package cliffberg.pddexample.serverlessclient;

import cliffberg.pddexample.compa.MergerGrpc;
import cliffberg.pddexample.compa.MergerGrpc.IdList;
import cliffberg.pddexample.compa.ID;

import cliffberg.pddexample.compb.CompB;

import com.amazonaws.services.lambda.runtime.Context;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;

import com.google.gson.JsonObject;

import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannel;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class Main implements RequestHandler<RequestClass, ResponseClass> {

    /**
    For Lambda deployment.
    Ref: https://docs.aws.amazon.com/lambda/latest/dg/java-handler-io-type-pojo.html
    */
    public static ResponseClass handleRequest(RequestClass request, Context context) {

        Regions clientRegion = Regions.DEFAULT_REGION;
        String bucketName = request.BucketName;
        String primKey = request.PrimaryFileKey;
        String secKey = request.SecondaryFileKey;
        String outKey = request.OutFileKey;

        AmazonS3 s3Client = AmazonS3ClientBuilder
            .standard().withRegion(clientRegion).withCredentials(new ProfileCredentialsProvider())
            .build();

        S3Object s3PrimObject = s3Client.getObject(new GetObjectRequest(bucketName, primKey));
        InputStream isPrim = s3PrimObject.getObjectContent();

        S3Object s3SecObject = s3Client.getObject(new GetObjectRequest(bucketName, secKey));
        InputStream isSec = s3SecObject.getObjectContent();

        // Create temp file. Note: if the data is sensitive, a protected directory should be used.
        File tempFile = File.createTempFile("", "");
        tempFile.deleteOnExit();
        OutputStream fileOut = new FileOutputStream(tempFile);

        main(filePrim, isSec, fileOut);
        isPrim.close();
        isSec.close();
        fileOut.close();

        /* Write to the output bucket */
        PutObjectRequest putReq = new PutObjectRequest(bucketName, outKey, tempFile);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType("plain/text");
        request.setMetadata(metadata);
        s3Client.putObject(putReq);

        return new ResponseClass();
    }

    /**
    For OpenWhisk deployment.
    Ref: https://openwhisk.apache.org/documentation.html#java
    */
    public static JsonObject main(JsonObject args) {
        /* Obtain arguments */
        String filePrimPath;
        String fileSecPath;
        String fileOutPath;
        try {
            filePrimPath = args.getAsJsonPrimitive("FilePrimPath").getAsString();
            fileSecPath = args.getAsJsonPrimitive("FileSecPath").getAsString();
            fileOutPath = args.getAsJsonPrimitive("FileOutPath").getAsString();
        } catch(Exception e) {
            throw new RuntimeException(ex);
        }

        /* Open file descriptors */
        InputStream filePrim;
        InputStream fileSec;
        OutputStream fileOut;
        try {
            filePrim = new FileInputStream(filePrimPath);
            fileSec = new FileInputStream(fileSecPath);
            fileOut = new FileOutputStream(fileOutPath);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        /* Call algorithm 1 */
        main(filePrim, fileSec, fileOut);

        /* Close descriptors */
        filePrim.close();
        fileSec.close();
        fileOut.close();

        /* Construct and return response */
        JsonObject response = new JsonObject();
        return response;
    }

    /*
    Algorithm 1:
    Ref: https://drive.google.com/open?id=1B9ZQ8Nn-4JMaqOR3dkf3tTDHS_yeepHciVqmu-qZrbs

    (Overview: Read a list of account IDs from each of two files, and combine them.
    Write the results to a third file. Duplicates are not allowed. Preserve the order
    of the items that were in the first list, and, secondarily, the order of items
    from the second list.)

    Given command line arguments fileNamePrim, fileNameSec, outputFileName;
    */
    private static void main(InputStream filePrim, InputStream fileSec, OutputStream fileOut) {

        /* Construct the two lists to be merged */
        List<ID> primaryList = buildList(filePrim);
        List<ID> secondaryList = buildList(fileSec);

        /* Construct arguments for gRPC call */
        IdList.Builder idListBuilder = IdList.newBuilder();
        IdList primaryIds = idListBuilder.addAllId(priimaryList);
        IdList secondaryIds = idListBuilder.addAllId(secondaryList);

        /* Call merge function via gRPC */
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        MergerGrpc.MergerBlockingStub blockingStub = MergerGrpc.newBlockingStub(channel);
        IdList result = blockingStub.merge(primaryList, secondaryList); // Calls REST service

        /* Convert returned value into a List */
        List<ID> mergedList = result.getIdList();

        // Write result to fileOut:
        PrintWriter pw = new PrintWriter(fileOut);
        for (ID id : mergedList) {
            pw.println(id.id);
        }
    }

    private static List<ID> buildList(InputStream file) {

        BufferedReader br = new BufferedReader(new InputStreamReader(file));

        List<ID> list = new LinkedList<ID>();
        for (;;) { // each line of file
            String str;
            try {
                str = br.readLine(); // to read a line from file;
            } catch (IOException iex) {
                break; // if no-more-lines, break from for.
            } catch (Exception ex) {
                throw new RuntimeException(ex); // if any other error, exit with error.
            }
            try {
                ID id = new ID(str); // parse a valid ID from line;
                CompB.addID(list, id);
            } catch (Exception ex) {
                System.err.println(ex.getMessage()); // log the error and continue with for.
            }
        }
        return list;
    }
}
